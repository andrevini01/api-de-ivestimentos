package com.api.investimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiInvestimentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiInvestimentosApplication.class, args);
	}

}
