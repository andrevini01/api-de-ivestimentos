package com.api.investimentos.models;

public class ResultadoSimulacao {

    private Double resultadoSimulacao;

    public ResultadoSimulacao() {
    }

    public Double getResultadoSimulacao() {
        return resultadoSimulacao;
    }

    public void setResultadoSimulacao(Double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }
}
