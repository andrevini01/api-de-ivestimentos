package com.api.investimentos.models;

import com.api.investimentos.enums.RiscoInvestimento;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
    public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @NotEmpty
    private String nome;

    @NotNull
    private String descricao;

    @NotNull
    private RiscoInvestimento riscoInvestimento;

    @NotNull
    @DecimalMin("0.1")
    private double porcentagemLucro;

    public Investimento() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public RiscoInvestimento getRiscoInvestimento() {
        return riscoInvestimento;
    }

    public void setRiscoInvestimento(RiscoInvestimento riscoInvestimento) {
        this.riscoInvestimento = riscoInvestimento;
    }

    public double getPorcentagemLucro() {
        return porcentagemLucro;
    }

    public void setPorcentagemLucro(double porcentagemLucro) {
        this.porcentagemLucro = porcentagemLucro;
    }
}
