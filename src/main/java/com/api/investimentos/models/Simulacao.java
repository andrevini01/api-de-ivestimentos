package com.api.investimentos.models;

import com.api.investimentos.enums.RiscoInvestimento;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private Integer investimentoId;

    @NotNull
    private Integer mesesDeAplicacao;

    @DecimalMin("100")
    private double dinheiroAplicado;

    public Simulacao() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(Integer investimentoId) {
        this.investimentoId = investimentoId;
    }

    public Integer getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(Integer mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}
