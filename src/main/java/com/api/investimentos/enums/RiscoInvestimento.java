package com.api.investimentos.enums;

public enum RiscoInvestimento {
    ALTO,
    BAIXO,
    MEDIO
}
