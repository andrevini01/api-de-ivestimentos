package com.api.investimentos.services;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Optional<Investimento> buscarInvestimento(Integer id){
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);
        return investimentoOptional;
    }

    public Iterable<Investimento> buscarTodosInvestimentos(){
        Iterable<Investimento> investimentoIterable = investimentoRepository.findAll();
        return investimentoIterable;
    }

    public Investimento atualizarInvestimento(Investimento investimento){
        Optional<Investimento> investimentoOptional = buscarInvestimento(investimento.getId());

        if (investimentoOptional.isPresent()){
            Investimento investimentoData = investimentoOptional.get();

            if (investimento.getNome() == null){
                investimento.setNome(investimentoData.getNome());
            }

            if (investimento.getDescricao() == null){
                investimento.setDescricao(investimentoData.getDescricao());
            }

            if (investimento.getRiscoInvestimento() == null){
                investimento.setRiscoInvestimento(investimentoData.getRiscoInvestimento());
            }

            if (investimento.getPorcentagemLucro() == 0){
                investimento.setPorcentagemLucro(investimentoData.getPorcentagemLucro());
            }
        }else{
            return new Investimento();
        }
        Investimento leadObjeto = investimentoRepository.save(investimento);
        return leadObjeto;
    }

    public void deletarInvestimento(Investimento investimento){
        investimentoRepository.delete(investimento);
    }
}
