package com.api.investimentos.services;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.models.ResultadoSimulacao;
import com.api.investimentos.models.Simulacao;
import com.api.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;

    @Autowired
    InvestimentoService investimentoService;

    public Simulacao salvarSimulacao(Simulacao simulacao){
        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);
        return simulacaoObjeto;
    }

    public ResultadoSimulacao calcularSimulacao(Simulacao simulacao){
        ResultadoSimulacao resultadoSimulacao = new ResultadoSimulacao();
        Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimento(simulacao.getInvestimentoId());

        if(investimentoOptional.isPresent()) {
            resultadoSimulacao.setResultadoSimulacao(
                    (simulacao.getDinheiroAplicado() * investimentoOptional.get().getPorcentagemLucro()) * simulacao.getMesesDeAplicacao());
        }

        return resultadoSimulacao;
    }

    public Iterable<Simulacao> buscarTodasSimulacoes(){
        Iterable<Simulacao> simulacaoIterable = simulacaoRepository.findAll();
        return simulacaoIterable;
    }
}
