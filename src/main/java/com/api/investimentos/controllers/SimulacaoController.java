package com.api.investimentos.controllers;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.models.ResultadoSimulacao;
import com.api.investimentos.models.Simulacao;
import com.api.investimentos.services.InvestimentoService;
import com.api.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @PutMapping("/")
    public ResponseEntity<ResultadoSimulacao> realizarSimulacao(@RequestBody @Valid Simulacao simulacao){
        Simulacao simulacaoObjeto = simulacaoService.salvarSimulacao(simulacao);
        ResultadoSimulacao resultadoSimulacao = simulacaoService.calcularSimulacao(simulacaoObjeto);
        if (resultadoSimulacao.getResultadoSimulacao() != null) {
            return ResponseEntity.status(200).body(resultadoSimulacao);
        }else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/")
    public Iterable<Simulacao> buscarTodasSimulacoes(){
        return simulacaoService.buscarTodasSimulacoes();
    }
}
