package com.api.investimentos.controllers;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping("/")
    public ResponseEntity<Investimento> salvarInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObjeto);
    }

    @GetMapping("/{id}")
    public Investimento buscarInvestimento(@PathVariable Integer id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimento(id);

        if(investimentoOptional.isPresent()){
            return investimentoOptional.get();
        }
        else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/")
    public Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoService.buscarTodosInvestimentos();
    }

    @PutMapping("/{id}")
    public Investimento atualizarInvestimento(@PathVariable Integer id, @RequestBody Investimento investimento){
        investimento.setId(id);
        Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimento);

        if(investimentoObjeto.getId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }else{
            return investimentoObjeto;
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Investimento> deletarInvestimento(@PathVariable Integer id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimento(id);

        if(investimentoOptional.isPresent()){
            investimentoService.deletarInvestimento(investimentoOptional.get());
            return ResponseEntity.status(204).body(investimentoOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}
