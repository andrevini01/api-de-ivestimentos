package com.api.investimentos.controllers;

import com.api.investimentos.enums.RiscoInvestimento;
import com.api.investimentos.models.Investimento;
import com.api.investimentos.services.InvestimentoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTests {
    @MockBean
    InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Investimento investimento;
    Optional<Investimento> investimentoOptionalMock;
    Iterable<Investimento> investimentoIterableMock;

    @BeforeEach
    public void inicializar(){
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Tesouro Direto");
        investimento.setDescricao("Aplicação em títulos do governo");
        investimento.setPorcentagemLucro(0.5);
        investimento.setRiscoInvestimento(RiscoInvestimento.BAIXO);

        investimentoOptionalMock = Optional.of((Investimento) investimento);
        investimentoIterableMock = Arrays.asList(investimento);
    }

    @Test
    public void testarSalvarInvestimento() throws Exception {
        Mockito.when(investimentoService.salvarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo("Aplicação em títulos do governo")));
    }

    @Test
    public void testarSalvarInvestimentoComDadosInvalidos() throws Exception {
        investimento.setNome("");
        Mockito.when(investimentoService.salvarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    @Test
    public void testarBuscarTodosInvestimentos() throws Exception{
        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(investimentoIterableMock);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Tesouro Direto")));

    }

    @Test
    public void testarBuscarInvestimento() throws Exception{
        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Tesouro Direto")));

    }

    @Test
    public void testarBuscarInvestimentoNoContent() throws Exception{
        investimentoOptionalMock = Optional.empty();
        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarInvestimento() throws Exception{
        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo("Aplicação em títulos do governo")));
    }

    @Test
    public void testarAtualizarInvestimentoInexistente() throws Exception{
        investimento = new Investimento();
        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarDeletarInvestimento() throws Exception{
        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);
        Mockito.doNothing().when(investimentoService).deletarInvestimento(Mockito.any(Investimento.class));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$.porcentagemLucro", CoreMatchers.equalTo(0.5)));
    }

    @Test
    public void testarDeletarLeadNoContent() throws Exception{
        investimentoOptionalMock = Optional.empty();
        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);
        Mockito.doNothing().when(investimentoService).deletarInvestimento(Mockito.any(Investimento.class));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
