package com.api.investimentos.controllers;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.models.ResultadoSimulacao;
import com.api.investimentos.models.Simulacao;
import com.api.investimentos.services.InvestimentoService;
import com.api.investimentos.services.SimulacaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.util.Arrays;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTests {

    @MockBean
    SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Simulacao simulacao = new Simulacao();
    ResultadoSimulacao resultadoSimulacao = new ResultadoSimulacao();
    Iterable<Simulacao> simulacaoIterableMock;

    @BeforeEach
    public void inicializar() {
        simulacao = new Simulacao();
        simulacao.setId(1);
        simulacao.setInvestimentoId(1);
        simulacao.setMesesDeAplicacao(10);
        simulacao.setDinheiroAplicado(100);

        resultadoSimulacao = new ResultadoSimulacao();
        resultadoSimulacao.setResultadoSimulacao(500.0);

        simulacaoIterableMock = Arrays.asList(simulacao);
    }

    @Test
    public void testarRealizarSimulacao() throws Exception{
        Mockito.when(simulacaoService.salvarSimulacao(Mockito.any(Simulacao.class))).thenReturn(simulacao);
        Mockito.when(simulacaoService.calcularSimulacao(Mockito.any(Simulacao.class))).thenReturn(resultadoSimulacao);

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultadoSimulacao", CoreMatchers.equalTo(500.0)));
    }

    @Test
    public void testarRealizarSimulacaoParaInvestimentoInexistente() throws Exception{
        resultadoSimulacao = new ResultadoSimulacao();

        Mockito.when(simulacaoService.salvarSimulacao(Mockito.any(Simulacao.class))).thenReturn(simulacao);
        Mockito.when(simulacaoService.calcularSimulacao(Mockito.any(Simulacao.class))).thenReturn(resultadoSimulacao);

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarBuscarTodasSimulacoes() throws Exception{
        Mockito.when(simulacaoService.buscarTodasSimulacoes()).thenReturn(simulacaoIterableMock);

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.get("/simulacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].dinheiroAplicado", CoreMatchers.equalTo(100.0)));

    }
}
