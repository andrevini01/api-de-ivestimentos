package com.api.investimentos.services;

import com.api.investimentos.enums.RiscoInvestimento;
import com.api.investimentos.models.Investimento;
import com.api.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTests {

    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    Investimento investimento;
    Optional<Investimento> investimentoOptionalMock;
    Iterable<Investimento> investimentoIterableMock;

    @BeforeEach
    public void inicializar(){
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Tesouro Direto");
        investimento.setDescricao("Aplicação em títulos do governo");
        investimento.setPorcentagemLucro(0.5);
        investimento.setRiscoInvestimento(RiscoInvestimento.BAIXO);

        investimentoOptionalMock = Optional.of((Investimento) investimento);
        investimentoIterableMock = Arrays.asList(investimento);
    }

    @Test
    public void testarSalvarInvestimento(){
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);

        Assertions.assertEquals(investimento, investimentoObjeto);
        Assertions.assertEquals(investimento.getPorcentagemLucro(), investimentoObjeto.getPorcentagemLucro());
    }

    @Test
    public void testarBuscarInvestimento(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptionalMock);

        Optional<Investimento> investimentoOptionalRetorno = investimentoService.buscarInvestimento(1);

        Assertions.assertEquals(investimentoOptionalRetorno, investimentoOptionalMock);
        Assertions.assertEquals(investimentoOptionalRetorno.get().getNome(), "Tesouro Direto");
    }

    @Test
    public void testarBuscarTodosInvestimentos(){
        Mockito.when(investimentoRepository.findAll()).thenReturn(investimentoIterableMock);

        Iterable<Investimento> investimentoIterableRetorno = investimentoService.buscarTodosInvestimentos();

        Assertions.assertEquals(investimentoIterableRetorno, investimentoIterableMock);
        Assertions.assertEquals(investimentoIterableRetorno.iterator().next().getNome(), "Tesouro Direto");
    }

    @Test
    public void testarAtualizarInvestimento(){
        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoRetorno = investimentoService.atualizarInvestimento(investimento);

        Assertions.assertEquals(investimentoRetorno, investimento);
        Assertions.assertEquals(investimentoRetorno.getNome(), investimento.getNome());
    }

    @Test
    public void testarAtualizarInvestimentoPassandoApenasNome(){
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("TesteMock");

        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoRetorno = investimentoService.atualizarInvestimento(investimento);

        Assertions.assertEquals(investimentoRetorno, investimento);
        Assertions.assertEquals(investimentoRetorno.getNome(), investimento.getNome());
    }

    @Test
    public void testarAtualizarInvestimentoPassandoApenasPorcentagemLucro(){
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setPorcentagemLucro(0.2);

        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoRetorno = investimentoService.atualizarInvestimento(investimento);

        Assertions.assertEquals(investimentoRetorno, investimento);
        Assertions.assertEquals(investimentoRetorno.getPorcentagemLucro(), investimento.getPorcentagemLucro());
    }

    @Test
    public void testarAtualizarInvestimentoRetornoVazio(){
        investimentoOptionalMock = Optional.empty();

        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);

        Investimento investimentoRetorno = investimentoService.atualizarInvestimento(investimento);

        Assertions.assertEquals(investimentoRetorno.getNome(), null);
    }

    @Test
    public void testarDeletarInvestimento(){
        investimentoService.deletarInvestimento(investimento);
        Mockito.verify(investimentoRepository, Mockito.times(1)).delete(Mockito.any(Investimento.class));
    }
}
