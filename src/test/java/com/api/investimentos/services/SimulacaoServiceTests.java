package com.api.investimentos.services;

import com.api.investimentos.enums.RiscoInvestimento;
import com.api.investimentos.models.Investimento;
import com.api.investimentos.models.ResultadoSimulacao;
import com.api.investimentos.models.Simulacao;
import com.api.investimentos.repositories.InvestimentoRepository;
import com.api.investimentos.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class SimulacaoServiceTests {
    @MockBean
    SimulacaoRepository simulacaoRepository;

    @MockBean
    InvestimentoService investimentoService;

    @Autowired
    SimulacaoService simulacaoService;

    Simulacao simulacao = new Simulacao();
    ResultadoSimulacao resultadoSimulacao = new ResultadoSimulacao();
    Iterable<Simulacao> simulacaoIterableMock;
    Investimento investimento;
    Optional<Investimento> investimentoOptionalMock;

    @BeforeEach
    public void inicializar() {
        simulacao = new Simulacao();
        simulacao.setId(1);
        simulacao.setInvestimentoId(1);
        simulacao.setMesesDeAplicacao(10);
        simulacao.setDinheiroAplicado(100);

        resultadoSimulacao = new ResultadoSimulacao();
        resultadoSimulacao.setResultadoSimulacao(500.0);

        simulacaoIterableMock = Arrays.asList(simulacao);

        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Tesouro Direto");
        investimento.setDescricao("Aplicação em títulos do governo");
        investimento.setPorcentagemLucro(0.5);
        investimento.setRiscoInvestimento(RiscoInvestimento.BAIXO);

        investimentoOptionalMock = Optional.of((Investimento) investimento);
    }

    @Test
    public void testarSalvarSimulacao(){
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).thenReturn(simulacao);

        Simulacao simulacaoObjeto = simulacaoService.salvarSimulacao(simulacao);

        Assertions.assertEquals(simulacao, simulacaoObjeto);
        Assertions.assertEquals(simulacao.getDinheiroAplicado(), simulacaoObjeto.getDinheiroAplicado());
    }

    @Test
    public void testarCalcularSimulacao(){
        Mockito.when(investimentoService.buscarInvestimento(Mockito.anyInt())).thenReturn(investimentoOptionalMock);

        ResultadoSimulacao resultadoSimulacaoRetorno = simulacaoService.calcularSimulacao(simulacao);

        Assertions.assertEquals(resultadoSimulacaoRetorno.getResultadoSimulacao(), 500);
    }

    @Test
    public void testarBuscarTodosSimulacoes(){
        Mockito.when(simulacaoRepository.findAll()).thenReturn(simulacaoIterableMock);

        Iterable<Simulacao> simulacaoIterableRetorno = simulacaoService.buscarTodasSimulacoes();

        Assertions.assertEquals(simulacaoIterableRetorno, simulacaoIterableMock);
        Assertions.assertEquals(simulacaoIterableRetorno.iterator().next().getMesesDeAplicacao(), 10);
    }
}
